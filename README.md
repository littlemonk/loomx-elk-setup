## ELK Setup ###

### Task 1 ### 

This has been achieved using `docker-compose` for solving the test problem.

In real world scenario - the use case if very different than the way it is handled here.  More on this in section Task 2. 

Here I have setup `filebeat`, `elasticsearch` and `kibana` on docker
containers and the complete setup is interacting with each other using `docker-compose`.

The `docker-compose` brings up three containers named `filebeat`, `elasticsearch` and `kibana`. 
The `logs` folder in this directory is mounted on `filebeat` container and the
file `sample.log` is used for sending the test data into `elasticsearch`.


### Task 2 ###

* Give us some thoughts on what you’ve done
* What are the single points of failure and how would you address them?

Elasticsearch is single node -- make it cluster
Elasticsearch cluster is down -- put some message broker in between filebeat and
elasticsearch (mostly kafka).
Kibana is down -- put kibana in HA. Deploy on multiple instances and put behind
load balancer. 
Filebeat is down -- Put monitoring. 


* What are the security concerns and how would you address them?

Currently, the data is flowing on http APIs -- put SSL on elasticsearch and
Kibana. Filebeat, Elasticsearch and Kibana all supports SSL out of the box. Data
can still  encrypted between filebeat to kafka and kafka to logstash
consumer using SSL.


### Bonus ###
We want to be logging all of our infrastructure across multiple regions and even providers,
but we don’t want our logging do be traveling over the internet unencrypted.
Create/use/implement a solution to encrypt between the origin instances and the destination ELK stack.

Bonus: Comment on another way this could be achieved, and how it might be better or worse.

Establish VPC peering or IPsec connection between multiple regions/providers.
Connection between IPsec connection/VPC peering is end to end encrypted. 


